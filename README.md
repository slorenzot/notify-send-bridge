#notify-send-brid

This Java Library allow send native notification to user using libnotify library from Linux distros.

##Example use:

First imports then:

```
#!java


import com.osm.integration.linux.GrowlNotify;
```


Then use:

```
#!java


GrowlNotify notify = new GrowlNotify();
notify.send("Notify Test on Linux", "This is a notify Test using "
            + "notify-send-bridge Java Library!");
```

The result:
![rsz_1screenshot_from_2016-09-26_21-03-21.png](https://bitbucket.org/repo/Bnr6ap/images/2745887999-rsz_1screenshot_from_2016-09-26_21-03-21.png)


**Next Step:** native windows notifications using Toast Library from Windows 8/8.1/10/+


##Credit
* Original author: slorenzot AT gmail DOT com
* Author's Blog: https://javaddictblog.wordpress.com/