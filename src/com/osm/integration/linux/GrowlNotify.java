/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osm.integration.linux;

import com.osm.integration.FileUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slorenzo
 */
public class GrowlNotify {
    
    public final String ALERT_ICON_PATH = "/com/osm/integration/icons/Alert.png";
    
    private final String NOTIFY_CMD = "notify-send";
    private String executablePath = "";
    
    private String iconExportedFullPath = "";
    
    public GrowlNotify() {
        this.executablePath = this.findPath(NOTIFY_CMD);
        
        try {
            iconExportedFullPath = FileUtils.ExportResource(ALERT_ICON_PATH);
        } catch (Exception ex) {
            Logger.getLogger(GrowlNotify.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }
    
    private String findPath(String searchCommand) {
        String customCommand[] = new String[] {
            "whereis",
            searchCommand
        };
        String output = this.executeCommand(customCommand);
        
        try {
            if (!output.isEmpty()) {
                // devuelve el path del comando customCommand
                return output.split(" ")[1];
            }
        } catch (Exception e) {
            Logger.getLogger(GrowlNotify.class.getName()).
                log(Level.SEVERE, "notify-send not found! Please, Install notify-send!");
        }
        
        return "";
    }
    
    private String executeCommand(String[] command) {
        final StringBuffer output = new StringBuffer();
        final Process p;
        
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
                    output.append(line).append("\n");
            }

        } catch (IOException | InterruptedException e) {
            Logger.getLogger(GrowlNotify.class.getName()).
                log(Level.SEVERE, "notify-send not found!");
        }

        return output.toString();
    }
    /**
     * Muestra el mensaje de notify-send sin icono
     * 
     * @param title
     * @param message 
     */
    public void send(String title, String message) {
        if (this.executablePath.isEmpty()) {
            Logger.getLogger(GrowlNotify.class.getName()).
                log(Level.SEVERE, "can't send user notify using notify-send!");
        }
        
        String customCommand[] = new String[] {
            this.executablePath,
//            "--icon=face-wink", 
//            "--urgency=critical",
            title, 
            message
        };
        
        this.executeCommand(customCommand);
    }
    /**
     * Muestra el mensaje de notify-send con un icono personalizado
     * 
     * @param iconPath
     * @param title
     * @param message 
     */
    public void send(String iconPath, String title, String message) {
        if (this.executablePath.isEmpty()) {
            Logger.getLogger(GrowlNotify.class.getName()).
                log(Level.SEVERE, "can't send user notify using notify-send!");
        }
        
        String customCommand[] = new String[] {
            this.executablePath,
            String.format("--icon=%s", iconExportedFullPath), 
//            "--urgency=critical",
            title, 
            message
        };
        
        this.executeCommand(customCommand);
    }
    
//    public void exportIconToTempdir() {
//        URL inputUrl = getClass().getResource("/absolute/path/of/source/in/jar/file");
//        File dest = new File("/path/to/destination/file");
//        FileUtils.copyURLToFile(inputUrl, dest);
//    }
    
}
