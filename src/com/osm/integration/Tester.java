/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osm.integration;

import com.osm.integration.linux.GrowlNotify;

/**
 *
 * @author slorenzo
 */
public class Tester {
    
    public static void main(String[] args) {
        
        GrowlNotify notify = new GrowlNotify();
        notify.send("Notify Test on Linux", "This is a notify Test using "
                + "notify-send-bridge Java Library!");
    }
    
}
